jQuery(document).ready(function(){

  var luckyNumber;

  //Set index numbers for dataSet

  var indexLinkedIn = 4;
  var indexPhoto = 3;
  var indexFirstName = 0;

  console.log(data[1]);

  //Load in all data
  var dtArray = [
    ['Pim','Bellinga',2013,'https://media.licdn.com/mpr/mpr/shrink_240_240/p/2/005/061/0e1/1fbd358.jpg','a'],
    ['Spencer','Heijnen',2013,'https://media.licdn.com/mpr/mpr/shrink_200_200/p/3/005/044/3f3/2b7e21b.jpg','a'],
    ['Harald','Tepper',0000,'https://media.licdn.com/mpr/mpr/shrink_200_200/p/6/000/26b/3c7/358995a.jpg','https://www.linkedin.com/in/haraldtepper'],


  ];
  //Present a random person
  function loadPage() {
    luckyNumber = Math.floor((Math.random() * dtArray.length));

    $('.inputVal').val('Voornaam');

    $('.message').text('Hee, jij bent toch..');

    $(".linkedInLink").attr("href", dtArray[luckyNumber][indexLinkedIn]);

    $('.mainWrapper').removeClass('goedAntwoord');
    $('.mainWrapper').removeClass('foutAntwoord');
    //$('.mainWrapper').css('background-color','#40255A');


    //alert(dtArray.length);
    //alert(luckyNumber);

    $('.personImage').css('background-image','url('+dtArray[luckyNumber][indexPhoto]+')');
    return;
  };

  loadPage();



  $('li').on('mouseenter','.inputVal',function(){

    $('.inputVal').val('');
  });

  $('li').on('click','.submit',function(){
    //get correct name
    var correctVal = dtArray[luckyNumber][indexFirstName];
    //alert(correctVal);

    //check name
    if($(this).closest('ul').find('.inputVal').val() == correctVal) {
      //alert('je hebt het goed!')
      $('.mainWrapper').addClass('goedAntwoord');
      //reset input box
      $(this).closest('ul').find('.inputVal').val('');
      $('.message').text('Klopt!')
      setTimeout(loadPage,1000);
    } else {
      //alert('helaas, klopt niet.. volgende keer beter!')
      $('.mainWrapper').addClass('foutAntwoord');
      //reset input box
      $(this).closest('ul').find('.inputVal').val('');
      $('.message').text('Eeh nee. Ik ben '+correctVal+'')
      setTimeout(loadPage,3000);
    }
    //$('h3').text("JA!");
  });

  $('li').on('click','.next',function(){
    //$('.mainWrapper').addClass('abb');
    //$('.mainWrapper').css('background-color','#40255A');

    loadPage();
  });
});
